package com.dolphy.servlet_04;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/readingSessionScope")
public class ReadingSessionScope extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter pw = response.getWriter();
		
		pw.println(request.getSession().getAttribute("fatih"));
		pw.println(request.getSession().getAttribute("hasan"));
		
		pw.close();
		
	}

}
