package com.dolphy.servlet_02;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/readingFile")
public class ReadingFileWithServletContext extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ServletContext context = getServletContext();
		
		InputStream txtStream = context.getResourceAsStream("/WEB-INF/fatih.txt");
		InputStream propStream = context.getResourceAsStream("/WEB-INF/naber.properties");
		
		Properties properties = new Properties();
		properties.load(propStream);
		
		System.out.println(properties.getProperty("name"));
		
		int content=0;
		ServletOutputStream output = response.getOutputStream();
		while((content=txtStream.read())!=-1) {
			output.write(content);
		}
		
		// PrintWriter'ı ServletOutputStream ile beraber kullanamayız.
//		PrintWriter w = response.getWriter();
//		w.write(" "+context.getRealPath("/WEB-INF/fatih.txt") + " ");
		
		// Aşşağıdaki txt dosyanın tamamen içeriğini bastı.
		URL url = context.getResource("/WEB-INF/fatih.txt");
		InputStream inputStream3 = url.openStream();
		int content2;
		while ((content2 = inputStream3.read()) != -1) {
			System.out.print((char) content2);
		}
		
		txtStream.close();
		propStream.close();
	}
}
