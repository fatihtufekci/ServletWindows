package com.dolphy.servlet_03;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/downloadingFile")
public class DownloadingFile extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("application/ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=test.xlsx");
		
		InputStream is = getServletContext().getResourceAsStream("/excel.xlsx");
		
		int read = 0;
		byte[] bytes = new byte[1024];
		
		ServletOutputStream sos = response.getOutputStream();
		
		while((read=is.read(bytes))!=-1) {
			sos.write(bytes, 0, read);
		}
		sos.close();
	}

}
