package com.dolphy.servlet_03;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/request")
public class Request extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter w = response.getWriter();
		w.write("<html><body>");
		w.write("Isminiz: " + request.getParameter("isim") + "</br>");
		w.write("Soyisminiz: " + request.getParameter("soyisim") + "</br>");
		w.write("Medeni Hali: " + request.getParameter("medeniHali") + "</br>");
		String[] diller = request.getParameterValues("progDili");
		w.write("Programlama Diliniz: ");
		// for(int i=0; i<diller.length; i++) {
		// w.write(" " + diller[i] + " ");
		// }
		for (String s : diller) {
			w.write(" " + s + " ");
		}
		w.write("</body></html>");

		Map<String, String[]> parameterMap = request.getParameterMap();

		for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
			System.out.println(entry.getKey() + " " + Arrays.asList(entry.getValue()));
		}
	}
}
