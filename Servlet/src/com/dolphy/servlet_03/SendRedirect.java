package com.dolphy.servlet_03;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/sendRedirect")
public class SendRedirect extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// relative path
		//response.sendRedirect("request.jsp");
		
		
		// absolute path
		response.sendRedirect("/Servlet/request.jsp");
	}
}
