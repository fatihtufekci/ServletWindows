package com.dolphy.servlet_03;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/responseHeader")
public class ResponseHeader extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html; charset=UTF-8");
		response.setHeader("araba", "BMW");
		response.setHeader("araba", "Mercedes");
		
		boolean contains = response.containsHeader("araba");
		System.out.println("Araba header'ı var mı: " + contains);
		
		String araba = response.getHeader("araba");
		
		// aynı key'de header eklememize yarar.
		response.addHeader("araba", "Toyata");
		response.addHeader("araba", "Ford");
		
		Collection<String> arabaHeader = response.getHeaders("araba");
		for(String s : arabaHeader) {
			System.out.println("araba: "+s);
		}
		
		response.setIntHeader("yas", 21);
		response.addIntHeader("yil", 2018);
		response.setDateHeader("bugun", System.currentTimeMillis());
		response.addDateHeader("yarin", 10000L);
		
		Collection<String> headerNames = response.getHeaderNames();
		for(String s:headerNames) {
			System.out.println(s);
		}
		
	}
}
