package com.dolphy.servlet_03;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/cookieResponse")
public class CookieResponse extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Cookie cookie1 = new Cookie("araba", "BMW");
		Cookie cookie2 = new Cookie("araba2","Mercedes");
		// Diğer yöntem
		response.addHeader("Set-Cookie", "araba3=Toyata");
		
		// 30 saniye sonra araba2 cookie'si silinecektir.
		cookie2.setMaxAge(15);
		
		response.addCookie(cookie1);
		response.addCookie(cookie2);

		Cookie[] cookies = request.getCookies();
		
		if(cookies!=null) {
			for(Cookie c : cookies) {
				System.out.println("Cookie adı: "+c.getName()+ "  Cookie değeri: "+ c.getValue());
			}
		}
		
	}


}
