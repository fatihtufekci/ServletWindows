package com.dolphy.servlet_03;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/moreRequest")
public class MoreRequest extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter w = response.getWriter();
		w.write("<html><body>");
		String serverName = request.getServerName();
		int localPort = request.getLocalPort(); // Sunucunun isteği tamamladığı port
		int serverPort = request.getServerPort(); // Sunucunun isteği aldığı port
		int remotePort = request.getRemotePort(); // Tarayıcının hangi porta istek sunduğu port
		String method = request.getMethod();
		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			w.write("<tr><td>" + headerName);
			w.write("<td>" + request.getHeader(headerName));
		}
		w.write("serverName: " + serverName+"</br>");
		w.write("localPort: " + localPort+"</br>");
		w.write("serverPort: " + serverPort+"</br>");
		w.write("remotePort: " + remotePort+ "</br>");
		w.write("method: " + method+ "</br>");
		w.write("</body></html>");
	}
}
